![](https://www.creseb.fr/voy_content/uploads/2023/06/Logo_Cydre_2023-1536x662.jpg#center "Application logo")

# Introduction

This application is intended for the seasonal streamflow forecasting of rivers within watersheds located in crystalline geological contexts.

## Getting started

Clone the repo:

`git clone https://gitlab.com/nicornette/cydre-dash.git`

Go to the application folder:

`cd cydre-dash`

Create conda environment and install python requirements:

`cd install`

`conda env create -f cydre.yml`

Activate the conda environment:

`conda activate cydre`

Go to the install folder and set the environment variable:

`python set_cydre_environment_variable.py`

Go to the launcher folder and run application:

`cd ../launchers`

`python run_interface.py`

Go to `http://localhost:8050/`

