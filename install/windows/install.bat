@echo off
:: Removes prompt at the beginning of the line
SETLOCAL ENABLEDELAYEDEXPANSION

echo NOTE: when a default value is proposed : press Enter to confirm this value, otherwise enter your own value.
echo.

set argC=0
for %%x in (%*) do Set /A argC+=1

:: Current directory
set INSTALL_DIR=%CD%
::ligne a changer si le batch n'est pas sur Cydre\install\windows\install.bat :
cd ..\..\..


::::::::::::::::::::::: ENVIRONMENT VARIABLES :::::::::::::::::::::::::::
echo Definition of user-defined environnement variables...
IF exist "%CYDRE_ROOT%" echo INFO: the fomer value of CYDRE_ROOT will be erased.
set CYDRE_ROOT=%CD%
setx CYDRE_ROOT "%CYDRE_ROOT%"
echo CYDRE_ROOT = %CYDRE_ROOT%
echo.


IF NOT "!CYDRE_RESULTS!"=="" GOTO cydre_results_ok
echo Enter the path to the Results of Cydre (will be created if non-existant): (Default: D:\results\Cydre)
set /P CYDRE_RESULTS=
IF "!CYDRE_RESULTS!"=="" set CYDRE_RESULTS=D:\results\Cydre
IF NOT exist "!CYDRE_RESULTS!" mkdir "!CYDRE_RESULTS!"
setx CYDRE_RESULTS "!CYDRE_RESULTS!"
echo CYDRE_RESULTS = !CYDRE_RESULTS!
echo.
:cydre_results_ok


IF NOT "!ANACONDA!"=="" GOTO anaconda_ok
echo Enter the path to the Anaconda Scripts: (Default: C:\ProgramData\Anaconda3\Scripts)
set /P ANACONDA=
IF "!ANACONDA!"=="" set ANACONDA=C:\ProgramData\Anaconda3\Scripts
IF NOT exist "!ANACONDA!" mkdir "!ANACONDA!"
setx ANACONDA "!ANACONDA!"
echo ANACONDA = !ANACONDA!
echo.
:anaconda_ok



echo Enter the python executable within the environment : (example: C:\Users\jdedreuz\.conda\envs\cydre4\python.exe)
echo Can be found with a where command within a command window in the environment 
set /P CYDRE_PYTHON=
setx CYDRE_PYTHON "!CYDRE_PYTHON!"
echo  CYDRE_PYTHON = !CYDRE_PYTHON!
echo.
:cydre_envt_ok

echo 


echo.
echo Script completed. 
echo Please logout you Windows session and relog it.
echo Then, to finish, continue the installation procedure described in the "installation.htm" document.

pause